package com.example.bmi

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.example.bmi.historyActivity.HistoryActivity
import com.example.bmi.logic.*
import java.lang.Double.parseDouble
import kotlin.math.round
import com.example.bmi.helpers.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var height: EditText
    private lateinit var weight: EditText
    private lateinit var resultBmiView: TextView
    private lateinit var bmiLevelView: TextView
    private lateinit var weightHeader: TextView
    private lateinit var heightHeader: TextView
    private lateinit var bmiCalculator: Bmi
    private var bmiForKgCmCalculator: BmiForKgCm = BmiForKgCm(0.0, 0.0)
    private var bmiForLbInCalculator: BmiForLbIn = BmiForLbIn(0.0, 0.0)
    private var resultColorCode: Int = 0
    private var isImperialSystem: Boolean = false
    private lateinit var sharedPreferences: HistorySharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.weight = weightInput
        this.height= heightInput
        this.resultBmiView = findViewById(R.id.resultBmi)
        this.bmiLevelView = findViewById(R.id.bmiLevel)
        this.weightHeader = weight_header
        this.heightHeader = height_header
        this.sharedPreferences = HistorySharedPreferences(this)
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main_options_menu, menu)

        val menuChangeUnits = menu.findItem(R.id.menu_change_units)
        menuChangeUnits.isChecked = this.isImperialSystem

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.menu_info -> {
                callInfoActivity()
                true
            }
            R.id.menu_change_units -> {
                this.isImperialSystem = !item.isChecked
                item.isChecked = this.isImperialSystem
                toggleHeadersUnits()
                recalculateInsertedValues()
                true
            }
            R.id.menu_about_me -> {
                callActivity(AboutMeActivity::class.java)
                true
            }
            R.id.menu_history -> {
                callActivity(HistoryActivity::class.java)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putCharSequence("resultBmi", this.resultBmiView.text)
        outState?.putCharSequence("bmiLevel", this.bmiLevelView.text)
        outState?.putString("weight", this.weight.text.toString())
        outState?.putString("height", this.height.text.toString())
        outState?.putString("weightHeader", this.weightHeader.text.toString())
        outState?.putString("heightHeader", this.heightHeader.text.toString())
        outState?.putBoolean("isImperialSystem", this.isImperialSystem)
        outState?.putInt("resultColorCode", this.resultColorCode)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        this.resultBmiView.text = savedInstanceState?.getCharSequence("resultBmi")
        this.bmiLevelView.text = savedInstanceState?.getCharSequence("bmiLevel")
        this.weight.setText(savedInstanceState?.getString("weight"))
        this.height.setText(savedInstanceState?.getString("height"))
        this.weightHeader.text = savedInstanceState?.getString("weightHeader")
        this.heightHeader.text = savedInstanceState?.getString("heightHeader")
        this.isImperialSystem = savedInstanceState!!.getBoolean("isImperialSystem")

        this.resultColorCode = savedInstanceState.getInt("resultColorCode")
        colorTexts(resultColorCode)
    }

    fun countBmiClicked(view: View) {
        if(!areInputsEmpty() && areInputsInRange()) {
            val countedBmi = this.bmiCalculator.countBmi()

            displayBmi(countedBmi)
            displayBmiLevel(Bmi.chooseObesityLevel(this, countedBmi))

            this.resultColorCode = Bmi.chooseColor(this, countedBmi)
            colorTexts(this.resultColorCode)
            addToSharedPreferences()
        }
    }

    private fun emptyResultTextViews() {
        emptyBmi()
        emptyBmiLevel()
    }

    private fun colorTexts(colorId: Int) {
        colorTextView(this.bmiLevelView, colorId)
        colorTextView(this.resultBmiView, colorId)
    }

    private fun displayBmiLevel(textToDisplay: String) {
        this.bmiLevelView.text = textToDisplay
    }

    private fun displayBmi(bmiToDisplay: Double) {
        this.resultBmiView.text = roundUp(bmiToDisplay, 2).toString()
    }

    private fun emptyBmi() {
        this.resultBmiView.text = getString(R.string.bmi_main_bmi_default)
    }

    private fun emptyBmiLevel() {
        this.bmiLevelView.text = getString(R.string.bmi_main_bmiLevel_default)
    }

    private fun roundUp(number: Double, decimalPlaces: Int): Double {
        val multiplier = Math.pow(10.0, decimalPlaces.toDouble())
        return round(number * multiplier) / multiplier
    }

    private fun toggleHeadersUnits() {
        changeWeightHeaderUnit()
        changeHeightHeaderUnit()
    }

    private fun changeWeightHeaderUnit() {
        if(this.isImperialSystem) {
            this.weightHeader.text = getString(R.string.bmi_main_weight_lb_header)
        }
        else {
            this.weightHeader.text = getString(R.string.bmi_main_weight_kg_header)
        }
    }

    private fun changeHeightHeaderUnit() {
        if(this.isImperialSystem) {
            this.heightHeader.text = getString(R.string.bmi_main_height_in_header)
        }
        else {
            this.heightHeader.text = getString(R.string.bmi_main_height_cm_header)
        }
    }

    private fun recalculateInsertedValues() {
        if(!this.weight.text.isEmpty()) {
            recalculateWeightValue()
        }
        if(!this.height.text.isEmpty()) {
            recalculateHeightValue()
        }
    }

    private fun recalculateWeightValue() {
        if(this.isImperialSystem) {
            val weightInLb = BmiForLbIn.convertKgToLb(parseDouble(this.weight.text.toString()))
            this.weight.setText(roundUp(weightInLb, 0).toString())
        }
        else {
            val weightInKg = BmiForKgCm.convertLbToKg(parseDouble(this.weight.text.toString()))
            this.weight.setText(roundUp(weightInKg, 0).toString())
        }
    }

    private fun recalculateHeightValue() {
        if(this.isImperialSystem) {
            val heightInIn = BmiForLbIn.convertCmToIn(parseDouble(this.height.text.toString()))
            this.height.setText(roundUp(heightInIn, 0).toString())
        }
        else {
            val heightInCm = BmiForKgCm.convertInToCm(parseDouble(this.height.text.toString()))
            this.height.setText(roundUp(heightInCm, 0).toString())
        }
    }

    private fun setCorrectBmiCalculator(weightValue: Double, heightValue: Double) {
        if(this.isImperialSystem) {
            this.bmiForLbInCalculator.weight = weightValue
            this.bmiForLbInCalculator.height = heightValue
            this.bmiCalculator = this.bmiForLbInCalculator
        }
        else {
            this.bmiForKgCmCalculator.weight = weightValue
            this.bmiForKgCmCalculator.height = heightValue
            this.bmiCalculator = bmiForKgCmCalculator
        }
    }

    private fun callInfoActivity() {
        val intent = Intent(this, InfoActivity::class.java)
        intent.putExtra("bmiLevel", this.resultBmiView.text)
        startActivity(intent)
    }

    private fun <T>callActivity(activityName: Class<T>) {
        val intent = Intent(this, activityName)
        startActivity(intent)
    }

    private fun addToSharedPreferences() {
        val historyRecord = HistoryObject(
            getCurrentDate(),
            this.weight.text.toString(), this.height.text.toString(),
            this.weightHeader.text.toString(), this.heightHeader.text.toString(),
            this.resultBmiView.text.toString(), this.bmiLevelView.text.toString(),
            this.resultColorCode
        )

        this.sharedPreferences.saveToHistory(historyRecord)
    }



    private fun areInputsEmpty(): Boolean {
        val isWeightEmpty = this.weight.text.isEmpty()
        val isHeightEmpty = this.height.text.isEmpty()
        if (isWeightEmpty || isHeightEmpty) {
            if (isWeightEmpty) {
                this.weight.error = getString(R.string.bmi_main_enter_weight_empty_error)
            }
            if (isHeightEmpty) {
                this.height.error = getString(R.string.bmi_main_enter_height_empty_error)
            }

            emptyResultTextViews()
            return true
        }
        return false
    }

    private fun areInputsInRange(): Boolean {
        val weightValue = parseDouble(this.weight.text.toString())
        val heightValue = parseDouble(this.height.text.toString())

        setCorrectBmiCalculator(weightValue, heightValue)

        val isWeightInRange = this.bmiCalculator.isWeightInRange(weightValue)
        val isHeightInRange = this.bmiCalculator.isHeightInRange(heightValue)

        if (isWeightInRange && isHeightInRange) {
            return true
        } else {
            emptyResultTextViews()

            if (!isWeightInRange) {
                showWeightInputErrors()
            }

            if (!isHeightInRange) {
                showHeightInputErrors()
            }

            return false
        }
    }

    private fun showWeightInputErrors() {
        this.weight.error =
            getString(
                R.string.bmi_main_enter_weight_incorrect_error,
                Math.ceil(bmiCalculator.MIN_WEIGHT).toString(),
                Math.ceil(bmiCalculator.MAX_WEIGHT).toString()
            )
    }

    private fun showHeightInputErrors() {
        this.height.error =
            getString(
                R.string.bmi_main_enter_height_incorrect_error,
                Math.ceil(bmiCalculator.MIN_HEIGHT).toString(),
                Math.ceil(bmiCalculator.MAX_HEIGHT).toString()
            )
    }
}

