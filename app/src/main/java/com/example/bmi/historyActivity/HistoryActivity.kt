package com.example.bmi.historyActivity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.LinearLayoutManager
import com.example.bmi.R
import com.example.bmi.logic.HistorySharedPreferences
import kotlinx.android.synthetic.main.activity_history.*

class HistoryActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var sharedPreferences: HistorySharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        this.sharedPreferences = HistorySharedPreferences(this)

        this.recyclerView = bmi_history_recycler_view
        this.linearLayoutManager = LinearLayoutManager(this)

        this.recyclerView.layoutManager = this.linearLayoutManager
        this.recyclerView.adapter = HistoryAdapter(sharedPreferences.getFromHistory())
    }
}
