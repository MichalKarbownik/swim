package com.example.bmi.historyActivity

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.bmi.R
import com.example.bmi.logic.HistoryObject
import com.example.bmi.helpers.colorTextView
import kotlinx.android.synthetic.main.history_record.view.*

class HistoryAdapter(val historyRecords: ArrayList<HistoryObject>): RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.history_record, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val reversePosition = itemCount - 1 - position

        holder.date.text = historyRecords[reversePosition].date
        holder.weight.text = historyRecords[reversePosition].weight
        holder.height.text = historyRecords[reversePosition].height
        holder.weightHeader.text = historyRecords[reversePosition].weightHeader
        holder.heightHeader.text = historyRecords[reversePosition].heightHeader
        holder.resultBmi.text = historyRecords[reversePosition].bmiValue
        holder.bmiLevel.text = historyRecords[reversePosition].bmiLevel

        val resultColorCode= historyRecords[reversePosition].resultColorCode
        colorTextView(holder.resultBmi, resultColorCode)
        colorTextView(holder.bmiLevel, resultColorCode)
    }

    override fun getItemCount(): Int = historyRecords.size

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val date: TextView = itemView.bmi_history_date
        val weight: TextView = itemView.bmi_history_weight
        val height: TextView = itemView.bmi_history_height
        val weightHeader: TextView = itemView.bmi_history_weight_header
        val heightHeader: TextView = itemView.bmi_history_height_header
        val resultBmi: TextView = itemView.bmi_history_bmi_result
        val bmiLevel: TextView = itemView.bmi_history_bmi_level
    }
}