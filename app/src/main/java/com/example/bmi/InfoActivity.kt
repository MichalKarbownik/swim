package com.example.bmi

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import com.example.bmi.helpers.colorTextView
import com.example.bmi.logic.Bmi
import kotlinx.android.synthetic.main.activity_info.*

class InfoActivity : AppCompatActivity() {

    private lateinit var bmiLevel: String
    private lateinit var title: TextView
    private lateinit var firstParagraph: TextView
    private lateinit var secondParagraph: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        this.bmiLevel = intent.extras?.getString("bmiLevel")!!
        this.title = bmi_info_title
        this.firstParagraph = bmi_info_first_paragraph
        this.secondParagraph = bmi_info_second_paragraph

        if(this.bmiLevel == getString(R.string.bmi_main_bmi_default)) {
            Toast.makeText(this, R.string.bmi_info_empty_bmi_level, Toast.LENGTH_LONG).show()
        }
        else {
            val bmiLevel = this.bmiLevel.toDouble()

            this.title.text = getString(R.string.bmi_info_title, Bmi.chooseObesityLevel(this, bmiLevel))
            colorTextView(this.title, Bmi.chooseColor(this, bmiLevel))
            this.firstParagraph.text = Bmi.chooseFirstParagraph(this, bmiLevel)
            this.secondParagraph.text = Bmi.chooseSecondParagraph(this, bmiLevel)
        }
    }
}
