package com.example.bmi

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_about_me.*

class AboutMeActivity : AppCompatActivity() {

    private val NUMBER_OF_IMAGES = 4
    private val CLICK_NUMBER_KEY = "clickNumber"
    private lateinit var bmiMeterImageView: ImageView
    private var clickNumber: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_me)

        this.bmiMeterImageView = bmi_meter
        this.bmiMeterImageView.setOnClickListener {bmiMeterClicked(it)}
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putInt(this.CLICK_NUMBER_KEY, this.clickNumber)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        this.clickNumber = savedInstanceState?.getInt(this.CLICK_NUMBER_KEY) ?: this.clickNumber
        this.bmiMeterImageView.setImageResource((chooseMeterImage()))
    }


    private fun bmiMeterClicked(view: View) {
        incrementNumberOfClicks()
        this.bmiMeterImageView.setImageResource(chooseMeterImage())
    }

    private fun incrementNumberOfClicks() {
        this.clickNumber++
        this.clickNumber %= NUMBER_OF_IMAGES
    }

    private fun chooseMeterImage(): Int {
        return when(clickNumber) {
            0 -> R.mipmap.bmi_meter_underweight
            1 -> R.mipmap.bmi_meter_normal
            2 -> R.mipmap.bmi_meter_overweight
            else -> R.mipmap.bmi_meter_obese
        }
    }
}
