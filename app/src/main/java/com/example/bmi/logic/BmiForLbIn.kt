package com.example.bmi.logic

class BmiForLbIn(var weight: Double, var height: Double) : Bmi {
    override val MIN_HEIGHT = BmiForKgCm.MIN_HEIGHT_CM / Bmi.cmInCoefficient
    override val MAX_HEIGHT = BmiForKgCm.MAX_HEIGHT_CM / Bmi.cmInCoefficient
    override val MIN_WEIGHT = BmiForKgCm.MIN_WEIGHT_KG / Bmi.kgLbCoefficient
    override val MAX_WEIGHT = BmiForKgCm.MAX_WEIGHT_KG / Bmi.kgLbCoefficient

    override fun countBmi() = (weight * 10000.0 * Bmi.kgLbCoefficient) / (height * Bmi.cmInCoefficient * height * Bmi.cmInCoefficient)

    override fun isHeightInRange(height: Double) = height in MIN_HEIGHT..MAX_HEIGHT
    override fun isWeightInRange(weight: Double) = weight in MIN_WEIGHT..MAX_WEIGHT

    companion object {
        fun convertKgToLb(weightInKg: Double): Double {
            return weightInKg / Bmi.kgLbCoefficient
        }

        fun convertCmToIn(heightInCm: Double): Double {
            return heightInCm / Bmi.cmInCoefficient
        }
    }
}