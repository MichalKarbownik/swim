package com.example.bmi.logic

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import kotlin.collections.ArrayList

const val PREFS_NAME = "bmiHistoryPrefs"
const val HISTORY_SIZE = 10

class HistorySharedPreferences(context: Context) {
    private val sharedPrefs: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    private val gson: Gson = Gson()

    fun saveToHistory(recordToSave: HistoryObject) {
        val sharedPrefsEditor = this.sharedPrefs.edit()
        val historyArray = this.getFromHistory()

        historyArray.add(recordToSave)
        maintainArrayListSize(historyArray, HISTORY_SIZE)

        val jsonToSave = gson.toJson(historyArray)
        sharedPrefsEditor.putString(PREFS_NAME, jsonToSave)
        sharedPrefsEditor.apply()
    }

    fun getFromHistory(): ArrayList<HistoryObject> {
        val sharedPrefsJson: String? = this.sharedPrefs.getString(PREFS_NAME, null)
        val type: Type? = object : TypeToken<ArrayList<HistoryObject>>() {}.type

        return gson.fromJson(sharedPrefsJson, type) ?: ArrayList()
    }

    private fun <T>maintainArrayListSize(array: ArrayList<T>, maximumSize: Int) {
        if(array.size > maximumSize) {
            array.removeAt(0)
        }
    }
}