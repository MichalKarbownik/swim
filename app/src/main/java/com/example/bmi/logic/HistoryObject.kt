package com.example.bmi.logic

data class HistoryObject(
    val date: String,
    val weight: String,
    val height: String,
    val weightHeader: String,
    val heightHeader: String,
    val bmiValue: String,
    val bmiLevel: String,
    val resultColorCode: Int
)