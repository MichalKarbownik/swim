package com.example.bmi.logic

import android.content.Context
import android.support.v4.content.ContextCompat
import com.example.bmi.R

interface Bmi {
    val MIN_HEIGHT: Double
    val MAX_HEIGHT: Double
    val MIN_WEIGHT: Double
    val MAX_WEIGHT: Double

    fun countBmi() : Double
    fun isHeightInRange(height: Double) : Boolean
    fun isWeightInRange(weight: Double) : Boolean

    companion object {
        enum class Units {
            METRIC, IMPERIAL
        }
        
        const val kgLbCoefficient = 0.45
        const val cmInCoefficient = 2.54

        fun chooseObesityLevel(context: Context, bmi: Double): String {
            return context.getString(chooseLevel(bmi).levelName)
        }

        fun chooseColor(context: Context, bmi: Double): Int {
            return ContextCompat.getColor(context, chooseLevel(bmi).color)
        }

        fun chooseFirstParagraph(context: Context, bmi: Double): String {
            return context.getString(chooseLevel(bmi).levelDescriptionFirstParagraph)
        }

        fun chooseSecondParagraph(context: Context, bmi: Double): String {
            return context.getString(chooseLevel(bmi).levelDescriptionSecondParagraph)
        }

        internal fun chooseLevel(bmi: Double): Level {
            for(level in Level.values())
                if(bmi < level.threshold)
                    return level

            return Level.values().last()
        }
    }
}

