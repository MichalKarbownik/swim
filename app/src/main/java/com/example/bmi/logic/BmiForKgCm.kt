package com.example.bmi.logic

class BmiForKgCm(var weight: Double, var height: Double) : Bmi{
    override val MIN_HEIGHT = MIN_HEIGHT_CM
    override val MAX_HEIGHT = MAX_HEIGHT_CM
    override val MIN_WEIGHT = MIN_WEIGHT_KG
    override val MAX_WEIGHT = MAX_WEIGHT_KG

    override fun countBmi() = weight * 10000.0 / (height * height)

    override fun isHeightInRange(height: Double) = height in MIN_HEIGHT..MAX_HEIGHT
    override fun isWeightInRange(weight: Double) = weight in MIN_WEIGHT..MAX_WEIGHT

    companion object {
        const val MIN_HEIGHT_CM = 125.0
        const val MAX_HEIGHT_CM = 225.0
        const val MIN_WEIGHT_KG = 40.0
        const val MAX_WEIGHT_KG = 150.0

        fun convertLbToKg(weightInLb: Double): Double {
            return weightInLb * Bmi.kgLbCoefficient
        }

        fun convertInToCm(heightInIn: Double): Double {
            return heightInIn * Bmi.cmInCoefficient
        }
    }
}
