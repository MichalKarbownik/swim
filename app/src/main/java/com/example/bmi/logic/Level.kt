package com.example.bmi.logic

import com.example.bmi.R

enum class Level {
    VERY_SEVERELY_UNDERWEIGHT {
        override val levelName = R.string.bmi_main_very_severely_underweight
        override val levelDescriptionFirstParagraph = R.string.bmi_main_underweight_description_first_paragraph
        override val levelDescriptionSecondParagraph = R.string.bmi_main_underweight_description_second_paragraph
        override val threshold = 15.0
        override val color = R.color.st_tropaz
    },
    SEVERELY_UNDERWEIGHT {
        override val levelName = R.string.bmi_main_severely_underweight
        override val levelDescriptionFirstParagraph = R.string.bmi_main_underweight_description_first_paragraph
        override val levelDescriptionSecondParagraph = R.string.bmi_main_underweight_description_second_paragraph
        override val threshold = 16.0
        override val color = R.color.st_tropaz
    },
    UNDERWEIGHT {
        override val levelName = R.string.bmi_main_underweight
        override val levelDescriptionFirstParagraph = R.string.bmi_main_underweight_description_first_paragraph
        override val levelDescriptionSecondParagraph = R.string.bmi_main_underweight_description_second_paragraph
        override val threshold = 18.5
        override val color = R.color.persian_green
    },
    NORMAL {
        override val levelName = R.string.bmi_main_normal
        override val levelDescriptionFirstParagraph = R.string.bmi_main_normal_description_first_paragraph
        override val levelDescriptionSecondParagraph = R.string.bmi_main_normal_description_second_paragraph
        override val threshold = 25.0
        override val color = R.color.japanese_laurel
    },
    OVERWEIGHT {
        override val levelName = R.string.bmi_main_overweight
        override val levelDescriptionFirstParagraph = R.string.bmi_main_overweight_description_first_paragraph
        override val levelDescriptionSecondParagraph = R.string.bmi_main_overweight_description_second_paragraph
        override val threshold = 30.0
        override val color = R.color.pirate_gold
    },
    MODERATELY_OBESE {
        override val levelName = R.string.bmi_main_moderately_obese
        override val levelDescriptionFirstParagraph = R.string.bmi_main_overweight_description_first_paragraph
        override val levelDescriptionSecondParagraph = R.string.bmi_main_overweight_description_second_paragraph
        override val threshold = 35.0
        override val color = R.color.pirate_gold
    },
    SEVERELY_OBESE {
        override val levelName = R.string.bmi_main_severely_obese
        override val levelDescriptionFirstParagraph = R.string.bmi_main_overweight_description_first_paragraph
        override val levelDescriptionSecondParagraph = R.string.bmi_main_overweight_description_second_paragraph
        override val threshold = 40.0
        override val color = R.color.guardsman_red
    },
    VERY_SEVERELY_OBESE {
        override val levelName = R.string.bmi_main_very_severely_obese
        override val levelDescriptionFirstParagraph = R.string.bmi_main_overweight_description_first_paragraph
        override val levelDescriptionSecondParagraph = R.string.bmi_main_overweight_description_second_paragraph
        override val threshold = 45.0
        override val color = R.color.guardsman_red
    },
    MORBIDLY_OBESE {
        override val levelName = R.string.bmi_main_morbidly_obese
        override val levelDescriptionFirstParagraph = R.string.bmi_main_overweight_description_first_paragraph
        override val levelDescriptionSecondParagraph = R.string.bmi_main_overweight_description_second_paragraph
        override val threshold = 50.0
        override val color = R.color.guardsman_red
    },
    SUPER_OBESE {
        override val levelName = R.string.bmi_main_super_obese
        override val levelDescriptionFirstParagraph = R.string.bmi_main_overweight_description_first_paragraph
        override val levelDescriptionSecondParagraph = R.string.bmi_main_overweight_description_second_paragraph
        override val threshold = 60.0
        override val color = R.color.guardsman_red
    },
    HYPER_OBESE {
        override val levelName = R.string.bmi_main_hyper_obese
        override val levelDescriptionFirstParagraph = R.string.bmi_main_overweight_description_first_paragraph
        override val levelDescriptionSecondParagraph = R.string.bmi_main_overweight_description_second_paragraph
        override val threshold = Double.MAX_VALUE
        override val color = R.color.guardsman_red
    };

    abstract val levelName: Int
    abstract val levelDescriptionFirstParagraph: Int
    abstract val levelDescriptionSecondParagraph: Int
    abstract val threshold: Double
    abstract val color: Int
}