package com.example.bmi.helpers

import java.text.DateFormat
import java.util.*


fun getCurrentDate(): String {
    val date = Date()
    val formatter = DateFormat.getDateTimeInstance()

    return formatter.format(date)
}