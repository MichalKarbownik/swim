package com.example.bmi.helpers

import android.widget.TextView

fun colorTextView(text: TextView, colorId: Int) {
    text.setTextColor(colorId)
}