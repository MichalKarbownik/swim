package com.example.bmi


import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun mainActivityTest() {
        displayTextViewsTest()
        displayButtonsTest()
        bmiCalculationTest()
    }

    @Test
    fun displayTextViewsTest() {
        onView(withId(R.id.weight_header))
            .check(matches(isDisplayed()))

        onView(withId(R.id.height_header))
            .check(matches(isDisplayed()))
    }

    @Test
    fun displayButtonsTest() {
        onView(withId(R.id.bmi_main_count_bmi_button))
            .check(matches(isDisplayed()))
    }

    @Test
    fun bmiCalculationTest() {
        onView(withId(R.id.weightInput))
            .perform(replaceText("50"), closeSoftKeyboard())

        onView(withId(R.id.heightInput))
            .perform(replaceText("150"), closeSoftKeyboard())

        onView(withId(R.id.bmi_main_count_bmi_button))
            .perform(click())

        onView(withId(R.id.resultBmi))
            .check(matches(withText("22.22")))
    }
}
