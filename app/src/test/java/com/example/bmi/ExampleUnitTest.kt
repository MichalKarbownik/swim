package com.example.bmi

import com.example.bmi.logic.Bmi
import com.example.bmi.logic.BmiForKgCm
import com.example.bmi.logic.BmiForLbIn
import com.example.bmi.logic.Level
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun for_valid_metric_data_should_count_bmi() {
        val bmi = BmiForKgCm(65.0, 170.0)
        assertEquals(22.49, bmi.countBmi(), 0.01)
    }

    @Test
    fun for_valid_imperial_data_should_count_bmi() {
        val bmi = BmiForLbIn(111.0, 63.0)
        assertEquals(19.51, bmi.countBmi(), 0.01)
    }

    @Test
    fun for_bmi_value_should_return_corresponding_bmi_level() {
        val level = Bmi.chooseLevel(18.0)
        assertEquals(Level.UNDERWEIGHT, level)
    }
}
